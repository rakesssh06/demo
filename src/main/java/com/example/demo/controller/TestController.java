package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
public class TestController {
    Logger logger = LoggerFactory.getLogger(TestController.class);
    @RequestMapping("api/v1/")
    public Map<String, Object> getValues(){
        logger.info("This is a Controller Class");
        Map<String, Object> data = new HashMap<>();
        data.put("message","Java is a fine langauge");
        data.put("langauge", Arrays.asList("java","Python","Angular"));
        data.put("code",369);
        return data;
    }
}
